
# ONT_SV_detection_workflow

## Global context of the workflow

The objectif of this pipeline is to use long reads from Nanopore sequencing to detect structural variants, more specifically, chromosomal inversions.

This pipeline use ONT data from 4 individuals of *coelopa frigida*, a seafly species as raw data. This species has 5 chromsomes and 1 small sexual one, and is known to have several large chromosomal inversions in it genome, such as on chromosome 1.

The problem here is to detect precisely the inversions and their specific breakpoints.
Using ONT and haplotagging data (see the [haplotagging pipeline](https://gitlab.com/Fantineb/haplotagging_sv_detection_workflow)) in parallel, we aim to characterise the localization of chromosomal inversions in this species.

## Prerequisites

For this project we used the Genouest cluster (for more information, see [general documentation](https://www.genouest.org/2017/03/02/cluster/)). Its aim is to provide access to many  bioinformatics resources and to run scripts, using a linux base.
Access to the cluster is via an ssh key. Once connected, the user can request the infrastructure via the SLURM submission system (for more information on SLURM, see [SLURM info](https://help.genouest.org/usage/slurm/)).

The computer used to run the analysis:

## How to use

Clone this repository to your local machine:

```git clone [repo-link]```

## Architecture of the project
This repository is composed by several and specific parts. 

* **01_scripts**: contains all scripts numbered in order of use. For almost each step, two scripts has been used. First an indi
* **02_genome**: contains the genome of reference used for alignment
* **03_environments**: contains all the environments used in this pipeline, each environement can be composed by severals dependant tools in a yaml file
* **04_raw_data**: contains the raw data send by the sequencing platform 

The other repository are described hereafter.


## Pipeline
### Overview

Step-by-step pipeline to detect chromosomal inversions using ONT data
IMAGE Pipeline

### Detailed steps

Here are all the steps implemented with each tool used:

The raw data are in fastq format. 

#### Step 1. Merging data:

Data are merged using `01_merge_fastq_file.sh` script for the individual version, and `01_merge_fastq_file_LOOP.sh` script for the loop version in `01_scripts` repository.

* **Input data**: several `fastq.gz` files from one sample

* **Output data**: one `fastq.gz` file per individuals

Merged data are saved in `05_raw_data_merged`repository.

#### Step 2. Quality control:

Quality control is performed by [Nanoplot](https://github.com/wdecoster/NanoPlot) (v1.42.0), using `nanoplot.yml` environment. 

The scripts used are `02_QC_nanoplot.sh` for the individual version, and `02_QC_nanoplot_LOOP.sh` for the loop version in `01_scripts` repository. 

* **Input data**: one `fastq.gz` file per sample

* **Output data**: QC files such as `.html`, `.png`, `.txt`

Quality control data are saved in `06_QC_nanoplot` repository.

#### Step 3. Trimming:

Trimming is performed by [Nanofilt](https://github.com/wdecoster/nanofilt) (v2.8.0), using `nanofilt.yml` environment. 

The scripts used are `03_filter_data_nanofilt.sh` for the individual version, and `03_filter_data_nanofilt_LOOP.sh` for the loop version in `01_scripts` repository. 

* **Input data**: merged `fastq.gz` file

* **Output data**: filtered `fastq.gz` file

Filtered data are saved in `07_filtered_data`.

#### Step 4. Quality control after trimming:

Quality control is performed by [Nanoplot](https://github.com/wdecoster/NanoPlot) (v1.42.0), using `nanoplot.yml` environment in `01_scripts` repository. 

The scripts used are `04_nanoplot_after_nanofilt.sh` for the individual version, and `04_nanoplot_after_nanofilt_LOOP.sh` for the loop version. 

* **Input data**: filtered `fastq.gz`

* **Output data**: QC files such as `.html`, `.png`, `.txt`

Quality control data are saved in `08_QC_nanoplot_filtered_data`  repository.

#### Step 5. Alignment on reference genome:

Alignment is performed by [Minimap2](https://github.com/lh3/minimap2) (v2.26-r1175), using `minimap2.yml` environment. Conversion and alignment statistics are performed by [bcftools](https://samtools.github.io/bcftools/bcftools.html) (v1.19), using `bcftools.yml`.

The scripts used are `05_alignment_on_reference_Minimap2.sh` for the individual version, and `05_alignment_on_reference_Minimap2_LOOP.sh` for the loop version in `01_scripts` repository.

* **Input data**: filtered `fastq.gz`

* **Output data**: `.sam`, `.bam`, `.bai`, `.txt`

Alignment data are saved in `09_alignment_on_reference_minimap2`  repository.

#### Step 6. Structural variant detection with Sniffles

SV calling is performed by [Sniffles](https://github.com/fritzsedlazeck/Sniffles) (v2.2), using `sniffles.yml` environment (dependencies are needed, see sniffles.yml). VCF filtering are performed by [bcftools](https://samtools.github.io/bcftools/bcftools.html) (v1.19), using `bcftools.yml` (dependencies are needed, see bcftools.yml).

The scripts used are `06_SV_detection.sh` for the individual version, and `06_SV_detection_LOOP.sh` for the loop version in `01_scripts` repository.

* **Input data**: `.bam` data from alignement

* **Output data**: `.vcf`

Quality control data are saved in `10_SV_detection_sniffles`  repository.

#### Step 7. Filtering the vcf file by position

 Filtering the vcf files by position is performed by [bcftools](https://samtools.github.io/bcftools/bcftools.html) (v1.19), using `bcftools.yml` (dependencies are needed, see bcftools.yml).

The script used is `
07_VCF_filter_bcftools.sh`.

* **Input data**: `.vcf`

* **Output data**: `.vcf`

Quality control data are saved in `11_VCF_filters_bcftools`  repository.


#### Step 7b. Filtering the vcf file by type of SVs

 Filtering the vcf files by the type of SVs is performed by [bcftools](https://samtools.github.io/bcftools/bcftools.html) (v1.19), using `bcftools.yml` (dependencies are needed, see bcftools.yml).

The scripts used are `07b_VCF_filter_typeSV_bcftools.sh` for the individual version, and `07b_VCF_filter_typeSV_bcftools_LOOP.sh` for the loop version in `01_scripts` repository.

* **Input data**: `.vcf`

* **Output data**: `.vcf`

Quality control data are saved in `11_VCF_filters_bcftools` repository.

#### Step 8. Structural variant detection with cuteSV

SV calling is performed by [cuteSV](https://github.com/tjiangHIT/cuteSV) (v2.1.0), using `cuteSV.yml` environment (dependencies are needed, see cuteSV.yml). VCF filtering are performed by [bcftools](https://samtools.github.io/bcftools/bcftools.html) (v1.19), using `bcftools.yml` (dependencies are needed, see bcftools.yml).

The scripts used are `08_SV_detection_cuteSV.sh` for the individual version, and `08_SV_detection_cuteSV_LOOP.sh` for the loop version in `01_scripts` repository.

* **Input data**: `.bam` data from alignement

* **Output data**: `.vcf`

Quality control data are saved in `12_SV_detection_cuteSV`  repository.


#### Step 9. VCF statistics

Statistics on vcf files are performed by [bcftools](https://samtools.github.io/bcftools/bcftools.html) (v1.19), using `bcftools.yml` (dependencies are needed, see bcftools.yml). 

The scripts used are `09_SV_stats.sh` for the individual version, and `09_SV_stats_LOOP.sh` for the loop version in `01_scripts` repository.

* **Input data**: `.vcf`

* **Output data**: `.txt`, `.vcf.gz`

Quality control data are saved in `13_VCF_stats`  repository.

#### Step 10. Structural variant detection with SVIM

SV calling is performed by [SVIM](https://github.com/eldariont/svim) (v2.0.0), using `svim.yml` environment (dependencies are needed, see svim.yml). Conversion and alignment statistics are performed by [bcftools](https://samtools.github.io/bcftools/bcftools.html) (v1.19), using `bcftools.yml` (dependencies are needed, see bcftools.yml).

The scripts used are `10_SV_detection_SVIM.sh` for the individual version, and `10_SV_detection_SVIM_LOOP.sh` for the loop version in `01_scripts` repository.

* **Input data**: `.bam` data from alignement

* **Output data**: `.vcf`

Quality control data are saved in `14_SV_detection_SVIM`  repository.

#### Step 11. Structural variant detection with Nanovar

SV calling is performed by [Nanovar](https://github.com/benoukraflab/NanoVar) (v1.5.1), using `nanovar.yml` environment (dependencies are needed, see nanovar.yml). VCF filtering are performed by [bcftools](https://samtools.github.io/bcftools/bcftools.html) (v1.19), using `bcftools.yml` (dependencies are needed, see bcftools.yml).

The scripts used are `11_SV_detection_nanovar.sh` for the individual version, and `11_SV_detection_nanovar_LOOP.sh` for the loop version in `01_scripts` repository.

* **Input data**: `.bam` data from alignement

* **Output data**: `.vcf`

Quality control data are saved in `15_SV_detection_nanovar`  repository.

#### Step 12. Merge VCF files from Sniffles, SVIM, Nanovar

Merging is performed by [Jasmine](https://github.com/mkirsche/Jasmine) (v1.1.5), using `jasmine.yml` environment (dependencies are needed, see jasmine.yml).

The scripts used are `12_merge_VCF.sh` for the individual version, and `12_merge_VCF_LOOP.sh` for the loop version in `01_scripts` repository.

* **Input data**: 3 `.vcf` files from sniffles, svim and nanovar

* **Output data**: one `.vcf` merged, and `.txt`

Quality control data are saved in `16_merged_VCF`  repository.

#### Step 13. SVs visualization using Samplot

visualization is performed by [Samplot](https://github.com/mkirsche/Jasmine) (v1.3.0), using `samplot.yml` environment.

The script used is `13_visualization_samplot.sh`.

* **Input data**: `.vcf` file

* **Output data**: `.png`, `.html`

Quality control data are saved in `17_visualization_samplot`  repository.


## Authors

[Fantine Benoit](https://gitlab.com/Fantineb)

Master students in bioinformatics at Rennes 1 University (Master mention Bio-informatique parcours Informatique pour la biologie et la santé).
This work has been realized during an internship at Ecobio laboratory, for [Claire Mérot](https://github.com/clairemerot) and [Claire Lemaitre](https://github.com/clemaitre), from Feb to July 2024.

## Sources

* [ONT_data_processing](https://github.com/LaurieLecomte/ONT_data_processing/tree/main)

* [SVs_long_reads](https://github.com/LaurieLecomte/SVs_long_reads)

## License
This project is under GNU AGPLv3 license.