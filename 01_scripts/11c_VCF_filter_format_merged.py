#python script vcf tableau
import sys

input_path = sys.argv[1]
output_path = sys.argv[2]

with open(input_path, "r") as file:
    with open(output_path, "w") as out:
        out.write("CHROM\tPOS\tEND\tDIFF\tSVLEN\n")
        for line in file:
            if line.startswith("#"):
                continue  

            split_line = line.split("\t")
            info = split_line[7].split(";")

            chrom = split_line[0]
            pos = int(split_line[1])
            keywords = []

            for item in info:
                if item.startswith("END="):
                    end = int(item.split('=')[1])
                elif item.startswith("IDLIST_EXT="):
                    idlist_ext = item.split('=')[1]
                    if "nv" in idlist_ext:
                        keywords.append("nanovar")
                    if "svim" in idlist_ext:
                        keywords.append("svim")
                    if "Sniffles2" in idlist_ext:
                        keywords.append("sniffles")

            diff = end - pos
            keywords_str = ",".join(keywords)

            out.write("{}\t{}\t{}\t{}\t{}\n".format(chrom, pos, end, diff, keywords_str))

