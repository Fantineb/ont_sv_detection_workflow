#python script vcf tab
import sys

input = sys.argv[1]
output = sys.argv[2]

with open(input, "r") as file:
    with open(output, "w") as out:
        out.write("CHROM \t START \t END \t SVLEN \n")
        for line in file:
            if line.startswith("#"):
                continue

            split_line = line.split("\t")
            info = split_line[7].split(";")

            a = info[2].split('=')[1]
            b = info[3].split('=')[1]

            out.write("{}\t{}\t{}\t{}\n".format(split_line[0], split_line[1], b, a))
	
