#!/bin/bash 

################### Script for QC on ONT and merged data  ###################
###################          Individual version           ###################

#Optional SLURM directives for users on HPC systems
#SBATCH --job-name=nanoplot
#SBATCH --partition=genouest
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=2
#SBATCH --time=1:00:00
#SBATCH --mem=5G
#SBATCH --output=nanoplot_%j.out
#SBATCH --error=nanoplot_%j.err

# Load conda environment with NanoPlot, to be changed with your own environment created before. The content of this environment: "03_environments/nanoplot.yml"
. /local/env/envconda.sh
conda activate Nanoplot

# VARIABLES
SAMPLE="C2"
CAT_DIR="05_raw_data_merged"
FASTQ_MERGED_FILE="$CAT_DIR/"$SAMPLE"*.fastq.gz" 
OUT_DIR="06_QC_nanoplot"


# Running NanoPlot to generate plots and statistics for Nanopore sequencing data
# Parameters:
# --plots dot: generates dot plots
# --legacy hex: option for the plot's appearance
# --title: sets the title of the plots
# --readtype 1D: indicates the type of Nanopore reads being analyzed
# --N50: calculates the N50 statistic
# --verbose: provides detailed information during the execution of NanoPlot

NanoPlot --fastq $FASTQ_MERGED_FILE --plots dot --legacy hex --title $SAMPLE -o $OUT_DIR --readtype 1D --N50 --verbose

conda deactivate
