#!/bin/sh
################### Script to run SVjedigraph  ###################


#Optional SLURM directives for users on HPC systems
#SBATCH --job-name=nanoplot
#SBATCH --partition=genouest
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=20
#SBATCH --time=2:00:00
#SBATCH --mem=30G
#SBATCH --output=nanoplot_%j.out
#SBATCH --error=nanoplot_%j.err


source /local/env/envconda.sh
conda activate /home/symbiose/clemaitr/bin/conda/svjedi-graph

VCF="AA59_sniffles_LG4_midbreakpoint.vcf"
GENOME="02_genome/genome.fasta"
FASTQ="07_filtered_data/AA59_filtered_Q10_L1000.fastq.gz"
pref="/groups/evolsv/SV_jedigraph/AA59_sniffles_LG4_breakpoint"
Nbthreads=20

svjedi-graph.py -v $VCF -r $GENOME -q $FASTQ -p $pref -t $Nbthreads > $pref.out


conda deactivate
