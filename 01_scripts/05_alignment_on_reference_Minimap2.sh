#!/bin/bash

################# Script for genome alignment with Minimap2  ###################
#################              Individual version            ###################

#Optional SLURM directives for users on HPC systems
#SBATCH --job-name=minimap2_alignment
#SBATCH --partition=genouest
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=4
#SBATCH --time=1:00:00
#SBATCH --mem=10G
#SBATCH --output=minimap2_alignment_%j.out
#SBATCH --error=minimap2_alignment_%j.err

# Load conda environment with Minimap2, to be changed with your own environment created before. The content of this environment: "03_environments/minimap2.yml"
. /local/env/envconda.sh
conda activate minimap2_env

# VARIABLES
SAMPLE="AA59"
GENOME="02_genome/genome.fasta"
FILT_DIR="07_filtered_data"
FASTQ_FILTERED="$FILT_DIR/"$SAMPLE"*.fastq.gz"
ALIGNED_DIR="09_alignment_on_reference_minimap2"
SAM_FILE="$ALIGNED_DIR/$SAMPLE.sam"
BAM_FILE="$ALIGNED_DIR/$SAMPLE.bam"

# Run Minimap2
minimap2 -ax map-ont $GENOME $FASTQ_FILTERED > $SAM_FILE

echo "alignment is done."

conda deactivate

. /local/env/envconda.sh
conda activate samtool_env

# Convert SAM into BAM files
samtools view -b $SAM_FILE | samtools sort -o $BAM_FILE

# Indexing the BAM file
samtools index $BAM_FILE

#Alignment statistics
samtools stats $BAM_FILE > $ALIGNED_DIR/"$SAMPLE"_mapping.stats

samtools coverage $BAM_FILE > $ALIGNED_DIR/"$SAMPLE"_coverage.txt


conda deactivate
