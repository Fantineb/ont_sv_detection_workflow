#!/bin/bash

##############    Script to create a statistic file with bcftools    ################
#################              Loop version            ###################

# Optional SLURM directives for users on HPC systems
#SBATCH --job-name=bcftools_stats_loop
#SBATCH --partition=genouest
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=2
#SBATCH --time=4:00:00
#SBATCH --mem=5G
#SBATCH --output=loop_bcftools_stats%j.out
#SBATCH --error=loop_bcftools_stats%j.err

# Load conda environment with bcftools, to be changed with your own environment created before. The content of this environment: "03_environments/bcftools.yml"
. /local/env/envconda.sh
conda activate bcftools_env

#VARIABLES
SAMPLE_LIST="sample_list.txt"
INPUT_DIR="10_SV_detection_sniffles"
OUTPUT_VCF="13_VCF_stats"

for SAMPLE in $(cat $SAMPLE_LIST); do

  INPUT_VCF="$INPUT_DIR/$SAMPLE/"$SAMPLE"*.vcf"
  bcftools sort $INPUT_VCF -Ov -o $OUTPUT_VCF/"$SAMPLE"_sorted.vcf

  # Compress the sorted VCF file with bgzip
  bgzip -c $OUTPUT_VCF/"$SAMPLE"_sorted.vcf > $OUTPUT_VCF/"$SAMPLE".vcf.gz

  # Indexing vcf file with tabix
  tabix -p vcf $OUTPUT_VCF/"$SAMPLE".vcf.gz

  # Path to the new file
  VCF_PATH=$OUTPUT_VCF/"$SAMPLE".vcf.gz

  # Use bcftools to obtain basic statistics from the VCF file
  bcftools stats $VCF_PATH > ${VCF_PATH%.vcf.gz}_stats_sniffles.txt

  # Informations about the variants
  bcftools query -f '%CHROM\t%POS\t%ID\t%REF\t%ALT\t%QUAL\t%FILTER\t%INFO\n'  $VCF_PATH > ${VCF_PATH%.vcf.gz}_variants_sniffles.txt

done


conda deactivate
