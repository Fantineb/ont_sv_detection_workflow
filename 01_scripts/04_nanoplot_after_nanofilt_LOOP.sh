#!/bin/bash

############# Script for QC on ONT filtered data with Nanoplot  ################
#############               Loop version                  ################

#Optional SLURM directives for users on HPC systems
#SBATCH --job-name=nanoplot_after_nanofilt
#SBATCH --partition=genouest
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=4
#SBATCH --time=4:00:00
#SBATCH --mem=15G
#SBATCH --output=nanoplot_after_nanofilt_%j.out
#SBATCH --error=nanoplot_after_nanofilt_%j.err

# Load conda environment with nanoplot, to be changed with your own environment created before. The content of this environment: "03_environments/nanoplot.yml"
. /local/env/envconda.sh
conda activate nanoplot_env

# VARIABLES
FILT_DIR="07_filtered_data"
SAMPLE_LIST="sample_list.txt"
OUT_DIR="08_QC_nanoplot_filtered_data"

for SAMPLE in $(cat $SAMPLE_LIST); do

  FASTQ_MERGED_FILE="$FILT_DIR/${SAMPLE}_filtered_Q10_L1000.fastq.gz"
  SAMPLE_OUT_DIR="${OUT_DIR}/${SAMPLE}"

  # Create output directory for the sample if it doesn't exist
  if [[ ! -d "$SAMPLE_OUT_DIR" ]]; then
    mkdir -p "$SAMPLE_OUT_DIR"
  fi

  # Running NanoPlot for each sample
  NanoPlot --fastq "$FASTQ_MERGED_FILE" --plots dot --legacy hex --title "$SAMPLE" -o "$SAMPLE_OUT_DIR" --readtype 1D --N50 --verbose

done


conda deactivate
