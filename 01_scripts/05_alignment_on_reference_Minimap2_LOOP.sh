#!/bin/bash

################# Script for genome alignment with Minimap2  ###################
#################                   Loop version               ###################

#SBATCH --job-name=minimap2_alignment_loop
#SBATCH --partition=genouest
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=8
#SBATCH --time=5:00:00 
#SBATCH --mem=50G
#SBATCH --output=minimap2_alignment_loop_%j.out
#SBATCH --error=minimap2_alignment_loop_%j.err

# Load conda environment with minimap2, to be changed with your own environment created before. The content of this environment: "03_environments/minimap2.yml"
. /local/env/envconda.sh
conda activate minimap2_env

# Variables
GENOME="02_genome/genome.fasta"
FILT_DIR="07_filtered_data"
ALIGNED_DIR="09_alignment_on_reference_minimap2"
SAMPLE_LIST="sample_list.txt"


for SAMPLE in $(cat $SAMPLE_LIST); do

  FASTQ_FILTERED="$FILT_DIR/${SAMPLE}*.fastq.gz"
  SAM_FILE="$ALIGNED_DIR/$SAMPLE.sam"
  BAM_FILE="$ALIGNED_DIR/$SAMPLE.bam"

  # Run Minimap2 for alignment
  minimap2 -ax map-ont -t20 $GENOME $FASTQ_FILTERED > $SAM_FILE
  
  conda deactivate

  # Switch to the samtools environment
  . /local/env/envconda.sh
  conda activate samtool_env

  # Convert SAM to BAM, sort, and index
  samtools view -b $SAM_FILE | samtools sort -o $BAM_FILE
  samtools index $BAM_FILE

  # Generate mapping statistics and coverage
  samtools stats $BAM_FILE > $ALIGNED_DIR/"$SAMPLE"_mapping.stats
  samtools coverage $BAM_FILE > $ALIGNED_DIR/"$SAMPLE"_coverage.txt

  echo  "Alignment done for $SAMPLE."
  
  # delete the SAM file to save space
  rm $SAM_FILE
  
  conda deactivate  
  . /local/env/envconda.sh
  conda activate minimap2_env
  
done
conda deactivate
