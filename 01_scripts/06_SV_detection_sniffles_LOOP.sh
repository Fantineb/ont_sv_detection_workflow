#!/bin/bash

################# Script for SV calling using Sniffles ###################
#################               Loop version              ###################

#SBATCH --job-name=SV_with_sniffles_loop
#SBATCH --partition=genouest
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=4
#SBATCH --time=2:00:00 
#SBATCH --mem=15G 
#SBATCH --output=SV_with_sniffles_loop%j.out
#SBATCH --error=SV_with_sniffles_loop%j.err

# Load conda environment with sniffles, to be changed with your own environment created before. The content of this environment: "03_environments/sniffles.yml"
. /local/env/envconda.sh
conda activate sniffles_env

# VARIABLES
GENOME="02_genome/genome.fasta"
INPUT_DIR="09_alignment_on_reference_minimap2"
OUTPUT_VCF="10_SV_detection_sniffles"
SAMPLE_LIST="sample_list.txt"

for SAMPLE in $(cat $SAMPLE_LIST); do
    mkdir -p "$OUTPUT_VCF/${SAMPLE}"
 
    INPUT_BAM="$INPUT_DIR/$SAMPLE.bam"
   
    # Run Sniffles
    sniffles -i "$INPUT_BAM" --reference "$GENOME" --minsvlen 50 --qc-coverage 4 --allow-overwrite -v "$OUTPUT_VCF/${SAMPLE}/${SAMPLE}_SV_calling.vcf"
  
    # Compressing the VCF file
    bgzip -c "$OUTPUT_VCF/${SAMPLE}/${SAMPLE}_SV_calling.vcf" > "$OUTPUT_VCF/${SAMPLE}/${SAMPLE}_SV_calling.vcf.gz"
    tabix -p vcf "$OUTPUT_VCF/${SAMPLE}/${SAMPLE}_SV_calling.vcf.gz"
  
    # Filter SV where POS > END
    bcftools filter -e "POS > INFO/END" "$OUTPUT_VCF/${SAMPLE}/${SAMPLE}_SV_calling.vcf.gz" > "$OUTPUT_VCF/${SAMPLE}/${SAMPLE}_filtered.vcf"

    # Sort the VCF file
    bcftools sort "$OUTPUT_VCF/${SAMPLE}/${SAMPLE}_filtered.vcf" -o "$OUTPUT_VCF/${SAMPLE}/${SAMPLE}_sorted.vcf"
  
    # Plot the main results
    python3 -m sniffles2_plot -i "$OUTPUT_VCF/${SAMPLE}/${SAMPLE}_sorted.vcf" -o "$OUTPUT_VCF/${SAMPLE}"
    echo "Sniffles is done for $SAMPLE"
done


conda deactivate
