#!/bin/bash

################### Script to merge fastq data from ONT ###################
###################         Loop version          ###################

#Optional SLURM directives for users on HPC systems
#SBATCH --job-name=nano_merge_loop
#SBATCH --partition=genouest
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=2
#SBATCH --time=1:00:00 
#SBATCH --mem=5G
#SBATCH --output=nano_merge_loop_%j.out
#SBATCH --error=nano_merge_loop_%j.err

# VARIABLES
CAT_DIR="05_raw_data_merged"
SAMPLE_LIST="sample_list.txt"

# Read sample names from file and process each
for SAMPLE in $(cat $SAMPLE_LIST); do
  
  RAW_DATA_DIR="/scratch/fbenoit/raw_data/nanopore/$SAMPLE"
  
  # Merge fastq files for each sample
  cat $RAW_DATA_DIR/*.fastq.gz > $CAT_DIR/"$SAMPLE"_merged_data.fastq.gz

done
