#!/bin/bash

################### Script for QC on ONT and merged data ###################
###################            Loop version             ###################

#Optional SLURM directives for users on HPC systems
#SBATCH --job-name=nanoplot_loop
#SBATCH --partition=genouest
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=4
#SBATCH --time=2:00:00 
#SBATCH --mem=15G
#SBATCH --output=nanoplot_loop_%j.out
#SBATCH --error=nanoplot_loop_%j.err

# Load conda environment with nanoplot, to be changed with your own environment created before. The content of this environment: "03_environments/nanoplot.yml"
. /local/env/envconda.sh
conda activate Nanoplot_env

# VARIABLES
CAT_DIR="05_raw_data_merged"
SAMPLE_LIST="sample_list.txt"
OUT_DIR="06_QC_nanoplot"

for SAMPLE in $(cat $SAMPLE_LIST); do

  FASTQ_MERGED_FILE="$CAT_DIR/${SAMPLE}_merged_data.fastq.gz"
  SAMPLE_OUT_DIR="${OUT_DIR}/${SAMPLE}"

  # Create output directory for the sample if it doesn't exist
  if [[ ! -d "$SAMPLE_OUT_DIR" ]]; then
    mkdir -p "$SAMPLE_OUT_DIR"
  fi

  # Running NanoPlot for each sample
  NanoPlot --fastq "$FASTQ_MERGED_FILE" --plots dot --legacy hex --title "$SAMPLE" -o "$SAMPLE_OUT_DIR" --readtype 1D --N50 --verbose

done

conda deactivate
