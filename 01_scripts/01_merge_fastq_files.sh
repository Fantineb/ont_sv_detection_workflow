#!/bin/bash

################### Script to merge fastq data from ONT ###################
###################        Individual version         ###################

#Optional SLURM directives for users on HPC systems
#SBATCH --job-name=nano_merge
#SBATCH --partition=genouest
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=2
#SBATCH --time=1:00:00
#SBATCH --mem=5G
#SBATCH --output=nano_merge_%j.out
#SBATCH --error=nano_merge_%j.err

# VARIABLES
SAMPLE="C2"
CAT_DIR="05_raw_data_merged"
RAW_DATA_DIR="/groups/evolsv/raw_data/Nanopore_2022/"$SAMPLE"/fastq_pass/"

# Merge fastq files for each individual
cat $RAW_DATA_DIR/*.fastq.gz > $CAT_DIR/"$SAMPLE"_merged_data.fastq.gz
