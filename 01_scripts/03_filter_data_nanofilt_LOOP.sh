#!/bin/bash

############## Script to filter raw ONT reads from merged files ################
##############                loop version                ################

#Optional SLURM directives for users on HPC systems
#SBATCH --job-name=nanofilt_loop
#SBATCH --partition=genouest
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=4
#SBATCH --time=2:00:00
#SBATCH --mem=15G
#SBATCH --output=nanofilt_loop_%j.out
#SBATCH --error=nanofilt_loop_%j.err

# Load conda environment with Nanofilt, to be changed with your own environment created before. The content of this environment: "03_environments/nanofilt.yml"
. /local/env/envconda.sh
conda activate nanofilt_env

# VARIABLES
SAMPLE_LIST="sample_list.txt"
MERGED_DATA="05_raw_data_merged"
FILT_DIR="07_filtered_data"

MIN_LEN=1000 # exclude reads shorter than MIN_LEN
MIN_QUAL=10 # exclude reads blow this MIN_QUAL threshold

for SAMPLE in $(cat $SAMPLE_LIST); do
  FASTQ_MERGED_FILE="$MERGED_DATA/"$SAMPLE"_merged_data.fastq.gz"
  FILTERED_DATA="$FILT_DIR/"$SAMPLE"_filtered_Q10_L1000.fastq.gz"

  # Run NanoFilt
  gunzip -c "$FASTQ_MERGED_FILE" | NanoFilt -q $MIN_QUAL -l $MIN_LEN --readtype 1D | gzip > "$FILTERED_DATA"

done

 
conda deactivate
