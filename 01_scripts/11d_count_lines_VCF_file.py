# -*- coding: utf-8 -*-
import sys

if len(sys.argv) < 2:
    print("Usage: python {} <TXT_FILE_PATH>".format(sys.argv[0]))    
    sys.exit(1)

input_path = sys.argv[1]

line_count = 0

with open(input_path, "r") as file:
    next(file)  
    for line in file:
        line_count += 1
print("Number of lines (excluding header) in file: {}".format(line_count))
