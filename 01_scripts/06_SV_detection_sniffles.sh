#!/bin/bash

#################    Script for SV calling using Sniffles    ###################
#################              Individual version            ###################

#Optional SLURM directives for users on HPC systems
#SBATCH --job-name=SV_with_sniffles
#SBATCH --partition=genouest
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=2
#SBATCH --time=1:00:00
#SBATCH --mem=5G
#SBATCH --output=SV_with_sniffles%j.out
#SBATCH --error=SV_with_sniffles%j.err

# Load conda environment with Sniffles, to be changed with your own environment created before. The content of this environment: "03_environments/sniffles.yml"
. /local/env/envconda.sh
conda activate sniffles_env

# VARIABLES
SAMPLE="BB22"
GENOME="02_genome/genome.fasta"
INPUT_DIR="09_alignment_on_reference_minimap2"
INPUT_BAM="$INPUT_DIR/"$SAMPLE".bam"
OUTPUT_VCF="10_SV_detection_sniffles"

# Run Sniffles
sniffles -i $INPUT_BAM --reference "$GENOME" --minsvlen 50 --qc-coverage 4  --allow-overwrite -v $OUTPUT_VCF/"$SAMPLE"_SV_calling.vcf

# Compressing the VCF file
bgzip -c "$OUTPUT_VCF/$SAMPLE"_SV_calling.vcf > "$OUTPUT_VCF/$SAMPLE"_SV_calling.vcf.gz
tabix -p vcf "$OUTPUT_VCF/$SAMPLE"_SV_calling.vcf.gz

# Filter SV where POS > END
bcftools filter -e "POS > INFO/END" "$OUTPUT_VCF/$SAMPLE"_SV_calling.vcf.gz > "$OUTPUT_VCF/$SAMPLE"_filtered.vcf

# Sort the VCF file
bcftools sort "$OUTPUT_VCF/$SAMPLE"_filtered.vcf -o "$OUTPUT_VCF/$SAMPLE"_sorted.vcf

# Plot the main results:
python3 -m sniffles2_plot -i "$OUTPUT_VCF/$SAMPLE"_sorted.vcf -o "$OUTPUT_VCF"


conda deactivate
