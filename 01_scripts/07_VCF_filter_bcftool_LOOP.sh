#!/bin/bash

############# Script to filter VCF files #########
#############        LOOP version       ##########

#Optional SLURM directives for users on HPC systems
#SBATCH --job-name=SV_filtered
#SBATCH --partition=genouest
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=2
#SBATCH --time=1:00:00
#SBATCH --mem=5G
#SBATCH --output=SV_filtered%j.out
#SBATCH --error=SV_filtered%j.err

# Load conda environment with bcftools
. /local/env/envconda.sh
conda activate bcftools_env

# Variables
REGION="LG4"
OUTPUT_VCF_DIR="11_VCF_filters_bcftools"
SAMPLE_LIST="sample_list.txt"
SV_TOOLS="SV_tools_text.txt" # File containing the list of directories for SV detection tools

# Read each sample name from SAMPLE_LIST and process
for SAMPLE in $(cat $SAMPLE_LIST); do
    # Process each SV tool directory
    for INPUT_DIR in $(cat $SV_TOOLS); do
       
        INPUT_VCF="${INPUT_DIR}/${SAMPLE}/${SAMPLE}_sorted.vcf"
        OUTPUT_VCF_COMPRESSED="${OUTPUT_VCF_DIR}/${SAMPLE}/${SAMPLE}_${INPUT_DIR}_LG4.vcf.gz"
        # Define final uncompressed VCF path
        FINAL_OUTPUT_VCF="${OUTPUT_VCF_DIR}/${SAMPLE}/${SAMPLE}_${INPUT_DIR}_LG4.vcf"

        # Ensure the sample-specific output directory exists
        mkdir -p "$(dirname "$OUTPUT_VCF_COMPRESSED")"

        # Compress the VCF file with bgzip for bcftools processing
        bgzip -c "$INPUT_VCF" > "$OUTPUT_VCF_COMPRESSED"
        # Index the compressed VCF file with tabix
        tabix -p vcf "$OUTPUT_VCF_COMPRESSED"
        # Filter with bcftools and keep the output compressed
        bcftools view "$OUTPUT_VCF_COMPRESSED" -r $REGION -Oz -o "$OUTPUT_VCF_COMPRESSED"
        # Decompress the filtered VCF file for final output
        gunzip -c "$OUTPUT_VCF_COMPRESSED" > "$FINAL_OUTPUT_VCF"
        # Remove the temporary compressed and indexed files if no longer needed
        rm "$OUTPUT_VCF_COMPRESSED" "$OUTPUT_VCF_COMPRESSED.tbi"
    done
done



conda deactivate
