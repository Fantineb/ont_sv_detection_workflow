#!/bin/bash

###########   Script to sort VCF file depending on SV type    ################
#################           Individual version            ###################

#Optional SLURM directives for users on HPC systems
#SBATCH --job-name=bcftools_SV_type
#SBATCH --partition=genouest
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=2
#SBATCH --time=1:00:00
#SBATCH --mem=5G
#SBATCH --output=SV_type_%j.out
#SBATCH --error=SV_type_%j.err

# Load conda environment with bcftools, to be changed with your own environment created before. The content of this environment: "03_environments/bcftools.yml"
. /local/env/envconda.sh
conda activate bcftools_env

# VARIABLES
SAMPLE="AA59"
INPUT_VCF="10_SV_detection_sniffles/"$SAMPLE"/"$SAMPLE"_sorted.vcf"
OUTPUT_VCF="11_VCF_filters_bcftools/"$SAMPLE"_sorted_INV_sniffles.vcf"

# Filter for inversions (SVs of type "INV") and sort the result
bcftools view -i 'SVTYPE="INV"' "$INPUT_VCF" | bcftools sort -o "$OUTPUT_VCF"

echo "Filtering and sorting are completed. The resulting file is: $OUTPUT_VCF"


conda deactivate
