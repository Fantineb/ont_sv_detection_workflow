#!/bin/bash

#################      Script for SV calling using Nanovar      ###################
#################              Loop version            ###################

#Optional SLURM directives for users on HPC systems
#SBATCH --job-name=nanovar_sv_calling_loop
#SBATCH --partition=genouest
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=4
#SBATCH --time=2:00:00
#SBATCH --mem=15G
#SBATCH --output=nanovar_loop%j.out
#SBATCH --error=nanovar_loop%j.err

# Load conda environment with Nanovar, to be changed with your own environment created before. The content of this environment: "03_environments/nanovar.yml"
source /local/env/envconda.sh
conda activate nanovar_env

# VARIABLES
SAMPLE_LIST="sample_list.txt"
INPUT_DIR="09_alignment_on_reference_minimap2"
OUTPUT_DIR="15_SV_detection_nanovar"
GENOME="02_genome/genome.fasta"

# Index the genome if it hasn't been indexed already
if [ ! -f "$GENOME.mmi" ]; then
    minimap2 -d "$GENOME.mmi" "$GENOME"
fi

for SAMPLE in $(cat $SAMPLE_LIST); do
 
  mkdir -p "$OUTPUT_DIR/$SAMPLE"
  INPUT_BAM="$INPUT_DIR/$SAMPLE.bam"

  # Run NanoVar
  nanovar -x ont -l 50 "$INPUT_BAM" "$GENOME" "$OUTPUT_DIR/$SAMPLE"

  echo "nanovar runned for $SAMPLE"

  # Compress the VCF file
  bgzip -c "$OUTPUT_DIR/$SAMPLE/$SAMPLE*.vcf" > "$OUTPUT_DIR/$SAMPLE/${SAMPLE}_SV_calling.vcf.gz"
  tabix -p vcf "$OUTPUT_DIR/$SAMPLE/${SAMPLE}_SV_calling.vcf.gz"
  
  echo "compression is done for $SAMPLE"

  # Switch environment to bcftools
  conda deactivate

  . /local/env/envconda.sh
  conda activate bcftools_env

  # Filter SVs where POS > END
  bcftools filter -e "POS > INFO/END" "$OUTPUT_DIR/$SAMPLE/${SAMPLE}_SV_calling.vcf.gz" > "$OUTPUT_DIR/$SAMPLE/${SAMPLE}_filtered.vcf"

  # Sort the VCF file
  bcftools sort "$OUTPUT_DIR/$SAMPLE/${SAMPLE}_filtered.vcf" -o "$OUTPUT_DIR/$SAMPLE/${SAMPLE}_sorted.vcf"

  conda deactivate
  . /local/env/envconda.sh
  conda activate nanovar_env

  echo "nanovar is done for $SAMPLE"
done

conda deactivate
