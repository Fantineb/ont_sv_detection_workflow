#!/bin/bash

###########   Script to plot the SVs using Samplot    ################

#Optional SLURM directives for users on HPC systems
#SBATCH --job-name=samplot
#SBATCH --partition=genouest
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=4
#SBATCH --time=1:00:00
#SBATCH --mem=5G
#SBATCH --output=samplot%j.out
#SBATCH --error=samplot%j.err

# Load conda environment with samplot, to be changed with your own environment created before. The content of this environment: "03_environments/samplot.yml"
. /local/env/envconda.sh
conda activate samplot_env

# VARIABLES
SAMPLE="BB22"
BAM_PATH="09_alignment_on_reference_minimap2/${SAMPLE}.bam"
VCF_PATH="10_SV_detection_sniffles/${SAMPLE}_sorted.vcf"
OUTPUT_DIR="17_visualization_samplot"

CHR="LG1"
START="9018899"
END="23295091"
SAMPLE_NAME="${SAMPLE}_${CHR}_${START}_${END}"
WINDOW=”3569043”

# Run Samplot
samplot plot \
    -n "$SAMPLE_NAME" \
    -b $BAM_PATH \
    -v $VCF_PATH \
    -o "${OUTPUT_DIR}/${SAMPLE_NAME}.png" \
    -c $CHR \
    -s $START \
    -e $END \
    -t "INV" \
    -w "$WINDOW" 

conda deactivate
