#!/bin/bash

###########   Script to sort VCF file depending on SV type    ################
#################           LOOP version            ###################


#Optional SLURM directives for users on HPC systems
#SBATCH --job-name=loop_bcftools_SV_type
#SBATCH --partition=genouest
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=4
#SBATCH --time=1:00:00
#SBATCH --mem=5G
#SBATCH --output=loop_SV_type_%j.out
#SBATCH --error=loop_SV_type_%j.err

# Load conda environment with bcftools, to be changed with your own environment created before. The content of this environment: "03_environments/bcftools.yml"
. /local/env/envconda.sh
conda activate bcftools_env

# VARIABLES
SAMPLE_LIST="sample_list.txt"
TOOLS=("sniffles" "nanovar" "svim") 
INPUT_DIRECTORIES=("10_SV_detection_sniffles" "14_SV_detection_nanovar" "15_SV_detection_SVIM")
OUTPUT_DIRECTORY="11_VCF_filters_bcftools"

for SAMPLE in $(cat $SAMPLE_LIST); do
    for i in "${!TOOLS[@]}"; do
        TOOL=${TOOLS[$i]}
        INPUT_DIR=${INPUT_DIRECTORIES[$i]}
        INPUT_VCF="$INPUT_DIR/$SAMPLE/${SAMPLE}_sorted.vcf"
        OUTPUT_VCF="$OUTPUT_DIRECTORY/${SAMPLE}_sorted_INV_${TOOL}.vcf"

        # Check if input VCF exists before attempting to filter and sort
        if [ -f "$INPUT_VCF" ]; then
            # Filter for inversions (SVs of type "INV") and sort the result
            bcftools view -i 'SVTYPE="INV"' "$INPUT_VCF" | bcftools sort -o "$OUTPUT_VCF"
            echo "Filtering and sorting are completed for $SAMPLE using $TOOL"
        else
            echo "Input VCF file not found for $SAMPLE using $TOOL: $INPUT_VCF"
        fi
    done
done


conda deactivate
