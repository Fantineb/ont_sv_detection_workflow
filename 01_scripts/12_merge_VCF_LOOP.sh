#!/bin/bash

###########   Script to use jasmine merge VCF files    ################
###########                LOOP version               ###############

#Optional SLURM directives for users on HPC systems
#SBATCH --job-name=jasmine
#SBATCH --partition=genouest
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=4
#SBATCH --time=1:00:00
#SBATCH --mem=5G
#SBATCH --output=jasmine%j.out
#SBATCH --error=jasmine%j.err

# Load conda environment with jasmine, to be changed with your own environment created before. The content of this environment: "03_environments/jasmine.yml"
. /local/env/envconda.sh
conda activate jasmine_env

# VARIABLES:
SAMPLE_LIST="sample_list.txt"  
VCF_DIR="11_VCF_filters_bcftools/INV"
OUTPUT_DIR="16_merged_VCF"
GENOME="02_genome/genome.fasta"

for SAMPLE in $(cat $SAMPLE_LIST); do
   
  VCF_LIST="${OUTPUT_DIR}/${SAMPLE}_vcfs_list.txt" # Create a VCF list for each sample
  
  # Build the list of VCFs for the current sample
  echo "${VCF_DIR}/${SAMPLE}_sorted_INV_nanovar.vcf" > $VCF_LIST
  echo "${VCF_DIR}/${SAMPLE}_sorted_INV_sniffles.vcf" >> $VCF_LIST
  echo "${VCF_DIR}/${SAMPLE}_sorted_INV_svim.vcf" >> $VCF_LIST

  # Run Jasmine for the current sample
  jasmine file_list=$VCF_LIST out_file="${OUTPUT_DIR}/${SAMPLE}_merged_inversions.vcf" genome_file=$GENOME max_dist_linear=95 min_support=2 --output_genotypes --use_end --threads=4
  
  echo "Jasmine is done for $SAMPLE"
done

conda deactivate
