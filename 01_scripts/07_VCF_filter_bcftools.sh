#!/bin/bash

############# Script to filter VCF files from Sniffles using bcftools  ################
#############                   Individual version                 ################

#Optional SLURM directives for users on HPC systems
#SBATCH --job-name=SV_filtered
#SBATCH --partition=genouest
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=2
#SBATCH --time=1:00:00
#SBATCH --mem=5G
#SBATCH --output=SV_filtered%j.out
#SBATCH --error=SV_filtered%j.err

# Load conda environment with Sniflles and bcftools, to be changed with your own environment created before. The content of this environment: "03_environments/sniffles.yml"
. /local/env/envconda.sh
conda activate sniffles_env

# VARIABLES
SAMPLE="AA59"
TOOLS="svim"
INPUT_DIR="11_VCF_filters_bcftools/"
INPUT_VCF="$INPUT_DIR"/"$SAMPLE"_sorted_INV_"$TOOLS".vcf
OUTPUT_VCF="11_VCF_filters_bcftools/$SAMPLE"
REGION="LG4"


# Compress the VCF file with bgzip
bgzip -c $INPUT_VCF > $OUTPUT_VCF/"$SAMPLE".vcf.gz

# Indexing vcf file with tabix
tabix -p vcf $OUTPUT_VCF/"$SAMPLE".vcf.gz

bcftools view $OUTPUT_VCF/"$SAMPLE".vcf.gz -r $REGION -Oz -o $OUTPUT_VCF/"$SAMPLE"_"$REGION"_"$TOOLS".vcf.gz

gunzip -c $OUTPUT_VCF/"$SAMPLE"_"$REGION"_"$TOOLS".vcf.gz > $OUTPUT_VCF/"$SAMPLE"_"$REGION"_"$TOOLS".vcf

rm "${OUTPUT_VCF}/${SAMPLE}_${REGION}_${TOOLS}.vcf.gz"



conda deactivate
