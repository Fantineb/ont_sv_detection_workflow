#!/bin/bash

#################      Script for SV calling using SVIM      ###################
#################              Loop version            #################

#Optional SLURM directives for users on HPC systems
#SBATCH --job-name=svim_sv_calling_loop
#SBATCH --partition=genouest
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=4
#SBATCH --time=2:00:00
#SBATCH --mem=5G
#SBATCH --output=svim_loop%j.out
#SBATCH --error=svim_loop%j.err

# Load conda environment with SVIM, to be changed with your own environment created before. The content of this environment: "03_environments/svim.yml"
source /local/env/envconda.sh
conda activate svim_env

# VARIABLES
SAMPLE_LIST="sample_list.txt"
INPUT_DIR="09_alignment_on_reference_minimap2"
OUTPUT_DIR="14_SV_detection_SVIM"
GENOME="02_genome/genome.fasta"

# Indexer le génome s'il n'est pas déjà indexé
if [ ! -f "$GENOME.fai" ]; then
    samtools faidx $GENOME
fi

for SAMPLE in $(cat $SAMPLE_LIST); do

  mkdir -p "$OUTPUT_DIR/${SAMPLE}"
  INPUT_BAM="$INPUT_DIR/$SAMPLE.bam"

  # Run SVIM
  svim alignment --min_sv_size 50 --minimum_depth 4 --insertion_sequences --read_names "$OUTPUT_DIR/$SAMPLE" $INPUT_BAM $GENOME
  
  # Compress VCF file
  bgzip -c "$OUTPUT_DIR/$SAMPLE/variants.vcf" > "$OUTPUT_DIR/$SAMPLE/${SAMPLE}_SVIM_SV_calling.vcf.gz"
  tabix -p vcf "$OUTPUT_DIR/$SAMPLE/${SAMPLE}_SVIM_SV_calling.vcf.gz"

  # Modify the environment to use bcftools
  conda deactivate
  . /local/env/envconda.sh
  conda activate bcftools_env

  # Filter SVs where POS > END
  bcftools filter -e "POS > INFO/END" "$OUTPUT_DIR/$SAMPLE/${SAMPLE}_SVIM_SV_calling.vcf.gz" > "$OUTPUT_DIR/$SAMPLE/${SAMPLE}_filtered.vcf"
  
  # Sort VCF files
  bcftools sort "$OUTPUT_DIR/$SAMPLE/${SAMPLE}_filtered.vcf" -o "$OUTPUT_DIR/$SAMPLE/${SAMPLE}_sorted.vcf"

  conda deactivate
  . /local/env/envconda.sh
  conda activate svim_env

  echo "SV call is done for $SAMPLE"
done

conda deactivate
