#!/bin/bash 

############## Script to filter raw ONT reads from merged files ################
##############                Individual version                ################

#Optional SLURM directives for users on HPC systems
#SBATCH --job-name=nanofilt
#SBATCH --partition=genouest
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=2
#SBATCH --time=1:00:00
#SBATCH --mem=5G
#SBATCH --output=nanofilt_%j.out
#SBATCH --error=nanofilt_%j.err

# Load conda environment with Nanofilt, to be changed with your own environment created before. The content of this environment: "03_environments/nanofilt.yml"
. /local/env/envconda.sh
conda activate nanofilt_env

# VARIABLES
SAMPLE="AA59"
MERGED_DATA="05_raw_data_merged"
FASTQ_MERGED_FILE="$MERGED_DATA/"$SAMPLE"*.fastq.gz" 
FILT_DIR="07_filtered_data"


MIN_LEN=1000 # exclude reads shorter than MIN_LEN
MIN_QUAL=10 # exclude reads blow this MIN_QUAL threshold

# Run NanoFilt
gunzip -c $FASTQ_MERGED_FILE | NanoFilt -q $MIN_QUAL -l $MIN_LEN --readtype 1D | gzip > $FILT_DIR/"$SAMPLE"_filtered_Q10_L1000.fastq.gz


conda deactivate
