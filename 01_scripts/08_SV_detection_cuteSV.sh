#!/bin/bash

#################    Script for SV calling using cuteSV    ###################
#################              Individual version            ###################

# Optional SLURM directives for users on HPC systems
#SBATCH --job-name=SV_with_cuteSV
#SBATCH --partition=genouest
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=2
#SBATCH --time=1:00:00
#SBATCH --mem=5G
#SBATCH --output=SV_with_cuteSV%j.out
#SBATCH --error=SV_with_cuteSV%j.err

# Load conda environment with cuteSV, to be changed with your own environment created before. The content of this environment: "03_environments/cuteSV.yml"
. /local/env/envconda.sh
conda activate cuteSV_env

# VARIABLES
SAMPLE="BB22"
INPUT_DIR="09_alignment_on_reference_minimap2"
INPUT_BAM="$INPUT_DIR/$SAMPLE.bam"
OUTPUT_VCF="12_SV_detection_cuteSV"
GENOME="02_genome/genome.fasta"

# indexing the genome
samtools faidx "$GENOME"

# Run cuteSV
cuteSV --min_size 50 $INPUT_BAM $GENOME $OUTPUT_VCF/"$SAMPLE"_SV_calling.vcf $OUTPUT_VCF 

conda deactivate
