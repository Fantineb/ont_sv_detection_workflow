#!/bin/bash
 
############# Script for QC on ONT filtered data with Nanoplot  ################
#############               Individual version                  ################

#Optional SLURM directives for users on HPC systems
#SBATCH --job-name=nanoplot_after_nanofilt
#SBATCH --partition=genouest
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=2
#SBATCH --time=1:00:00
#SBATCH --mem=5G
#SBATCH --output=nanoplot_after_nanofilt_%j.out
#SBATCH --error=nanoplot_after_nanofilt_%j.err

# Load conda environment with nanoplot, to be changed with your own environment created before. The content of this environment: "03_environments/nanoplot.yml"
. /local/env/envconda.sh
conda activate nanoplot_env

# VARIABLES
SAMPLE="BB22"
FILT_DIR="07_filtered_data"
FASTQ_FILTERED="$FILT_DIR/"$SAMPLE"*.fastq.gz"
OUT_DIR="08_QC_nanoplot_filtered_data"

# Run NanoPlot
NanoPlot --fastq $FASTQ_FILTERED --plots dot --legacy hex --title $SAMPLE -o $OUT_DIR --readtype 1D --N50 --verbose


conda deactivate
