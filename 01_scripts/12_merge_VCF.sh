#!/bin/bash

###########   Script to use jasmine merge VCF files    ################

#Optional SLURM directives for users on HPC systems
#SBATCH --job-name=jasmine
#SBATCH --partition=genouest
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=4
#SBATCH --time=1:00:00
#SBATCH --mem=5G
#SBATCH --output=jasmine%j.out
#SBATCH --error=jasmine%j.err


# Load conda environment with jasmine, to be changed with your own environment created before. The content of this environment: "03_environments/jasmine.yml"
. /local/env/envconda.sh
conda activate jasmine_env

# VARIABLES:
VCF_DIR="11_VCF_filters_bcftools/INV"
SAMPLE="AA59"
OUTPUT_DIR="16_merged_VCF"
GENOME="02_genome/genome.fasta"
VCF_LIST="${OUTPUT_DIR}/vcfs_list.txt" # list of VCF files
echo "${VCF_DIR}/${SAMPLE}_sorted_INV_nanovar.vcf" > $VCF_LIST
echo "${VCF_DIR}/${SAMPLE}_sorted_INV_sniffles.vcf" >> $VCF_LIST
echo "${VCF_DIR}/${SAMPLE}_sorted_INV_svim.vcf" >> $VCF_LIST

# Run Jasmine
jasmine file_list=$VCF_LIST out_file="${OUTPUT_DIR}/${SAMPLE}_merged_inversions_max_dist_04.vcf" genome_file=$GENOME max_dist_linear=0.4 min_support=2 --output_genotypes --use_end --threads=4

conda deactivate
